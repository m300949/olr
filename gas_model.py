# Set path to line catalogue
import os
if "ARTS_DATA_PATH" not in os.environ:
    os.environ["ARTS_DATA_PATH"] = "/pf/m/m300912/ClimateEffects_Scripts/ARTS/artscourse/"

from absorption_module import tag2tex, calculate_absxsec
from ptModel import pt_model
import physical_constants as phc
import pyarts
import numpy as np
import typhon as ty

class gas_model:
    """class loads data needed to calculate optical depth."""

    #gas = "H20" for example
    #crosssection_array = np.array()
    #frequencies = np.array()
    #ptmodel
    #spec_mass_prof



    def __init__(self, gas_array, ptmodel, fnum=1000):
        """dataloaders load data for set of gases, e.g. ["o2","co2"]"""
        self.gas_array = gas_array
        self.ptmodel = pt_model()
        self.ws = self.__initialize_ARTS_ws(fnum = fnum)

         
    
        #return  
    

    def __initialize_ARTS_ws(self,
        fnum=1000, 
        fmin=1.0, 
        fmax=75e12,
        lineshape="LP",
        normalization="RQ",
        verbosity=0
    ):
        """Calculate absorption cross sections.

        Parameters:
            species (str): Absorption species name.
            pressure (float): Atmospheric pressure [Pa].
            temperature (float): Atmospheric temperature [K].
            fmin (float): Minimum frequency [Hz].
            fmax (float): Maximum frequency [Hz].
            fnum (int): Number of frequency grid points.
            lineshape (str): Line shape model.
                                Available options:
                                DP   	 - 	 Doppler profile,
                                LP   	 - 	 Lorentz profile,
                                VP   	 - 	 Voigt profile,
                                SDVP 	 - 	 Speed-dependent Voigt profile,
                                HTP  	 - 	 Hartman-Tran profile.
            normalization (str): Line shape normalization factor.
                                Available options:
                                VVH  	 - 	 Van Vleck and Huber,
                                VVW  	 - 	 Van Vleck and Weisskopf,
                                RQ   	 - 	 Rosenkranz quadratic,
                                None 	 - 	 No extra normalization.  
            verbosity (int): Set ARTS verbosity (``0`` prevents all output).

        Returns:
            ndarray, ndarray: Frequency grid [Hz], Abs. cross sections [m^2]
        """
        myspecies = self.gas_array
        
        # Create ARTS workspace and load default settings
        ws = pyarts.workspace.Workspace(verbosity=0)
        ws.execute_controlfile("general/general.arts")
        ws.execute_controlfile("general/continua.arts")
        ws.execute_controlfile("general/agendas.arts")
        ws.verbositySetScreen(ws.verbosity, verbosity)

        # We do not want to calculate the Jacobian Matrix
        ws.jacobianOff()

        # Agenda for scalar gas absorption calculation
        ws.Copy(ws.abs_xsec_agenda, ws.abs_xsec_agenda__noCIA)

        # Define absorption species
        ws.abs_speciesSet(species= myspecies)
        ws.ArrayOfIndexSet(ws.abs_species_active, [0])

        ws.abs_lines_per_speciesReadSpeciesSplitCatalog(
           basename="spectroscopy/Artscat/"
        )

        ws.abs_lines_per_speciesSetLineShapeType(option=lineshape)
        ws.abs_lines_per_speciesSetCutoff(option="ByLine", value=750e9)
        ws.abs_lines_per_speciesSetNormalization(option=normalization)

        # Create a frequency grid
        ws.VectorNLinSpace(ws.f_grid, fnum, fmin, fmax)

        # Throw away lines outside f_grid
        ws.abs_lines_per_speciesCompact()

        # Atmospheric settings
        ws.AtmosphereSet1D()
        ws.stokes_dim = 1
        
        # isotop
        ws.isotopologue_ratiosInitFromBuiltin()
        
        return ws

    def set_ws_height(self, height):
        """Set height for the ARTS ws model"""
        self.pressure = self.ptmodel.get_pressure_array(height)
        self.temperature = self.ptmodel.get_temperature_array(height)
        mixing_ratio = self.ptmodel.get_mixing_ration_array(self.gas_array, height)

        # Setting the pressure, temperature and vmr
        self.ws.rtp_pressure = float(self.pressure)  # [Pa]
        self.ws.rtp_temperature = float(self.temperature)  # [K]
        self.ws.rtp_vmr = np.array(mixing_ratio)  # [VMR]
        self.ws.Touch(self.ws.rtp_nlte)

        
    def get_abs_xsec(self):
        # Calculate absorption cross sections
        self.ws.propmat_clearsky_agenda_checked = 1
        self.ws.lbl_checkedCalc()
        self.ws.propmat_clearskyInit()
        self.ws.propmat_clearskyAddLines()

        # Convert abs coeff to cross sections on return
        number_density = self.pressure / (ty.constants.boltzmann * self.temperature)
        return self.ws.propmat_clearsky.value.data.data[0, 0, :, 0].copy()# / number_density

    def get_frequency_grid(self):
        # Calculate absorption cross sections
        #self.ws.propmat_clearsky_agenda_checked = 1
        #self.ws.lbl_checkedCalc()
        #self.ws.propmat_clearskyInit()
        #self.ws.propmat_clearskyAddLines()

        # Convert abs coeff to cross sections on return
        #number_density = pressure / (ty.constants.boltzmann * temperature)
        return self.ws.f_grid.value.copy()

    def calculate_cross_section_kv(self, low_resolution_height_array):
        """loads cross section for gases"""
        
        freq = self.get_frequency_grid()
        abs_xsec_array = np.zeros((len(low_resolution_height_array),len(freq)))
        iterator = 0
        for height in low_resolution_height_array:
            self.set_ws_height(height) # Sets ws height
            #print(height)
            #print(type(self.get_abs_xsec()))
            #print(np.shape(self.get_abs_xsec()))
            abs_xsec_array[iterator,:] = np.array(self.get_abs_xsec())[:] # Gets kv-array for height and appends to abs_xsec_array
            iterator += 1
        
        return freq, abs_xsec_array

    def get_xsec_high_res_array(self, height_array, resolution):
        """returns optical depth of gas on height, for a frequency"""
        spare = int(len(height_array)/resolution)
        height_spare = height_array[:spare:]
        
        freq, lowres_abs_xsec_array = self.calculate_cross_section_kv(height_spare)
        heighres_abs_xsec_array = np.interp(height_array,height_spare,lowres_abs_xsec_array)
        return freq, heighres_abs_xsec_array
        

    # def __get_specific_weight(self,height, density_rho):
    #     """y = (m*g)/v = rho*g """
    #     #universal gravity constant:
    #     g = 6.67*10**(-11)
    #     #mass of earth:
    #     m = 5.972*10**24
    #     #earh radius:
    #     r = 6400*10**3
    #     #calculate local graviation:
    #     g = g*m/(height+r)**2
    #     return g*density_rho
    #
    # def __get_density(self, pressure):
    #     """ideal gas law applied"""
    #     #pv=nkbt
    #     pass



    
   
