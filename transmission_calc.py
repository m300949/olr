from ptModel import pt_model
from gas_model import gas_model
from absorption_module import tag2tex, calculate_absxsec
from ptModel import pt_model
import physical_constants as phc
import pyarts
import numpy as np
import typhon as ty

class radiance_calculator:
    """Class calculates radiance for different gases or surface temperatures"""

    gas_array = ['Ar', 'O2', 'N2', 'CO2', 'CH4', 'H2O', 'N2O', 'O3']

    def __init__(self, gas_array = None, ptModel = None, z = [],
                    h_res = 100, h_min = 0, h_max = 13.e3, ground_temp = None, x_CO2 = None, fnum=1000):
        """Creates an array of gas models from the gas_model class
            and accordingly a p_t_model for the troposphere. gas_array should be a list of strings.
            Default values are h_res = 100, ground_temp = 300, h_min = 0,
            h_max is adjusted according to ground_temp with a reference height of 15km.
            Please provide every input in SI units"""

        ####    SETUP P-T-MODEL     #####
        #initialize p_t_model and adopt changes, if surface temperature has changed:
        if not ptModel: self.ptModel        = pt_model()
        if ground_temp: self.ptModel.set_ground_temperature(ground_temp)

        if x_CO2:       self.ptModel.x_CO2  = x_CO2 # mixing ratio of CO2 is taken from phc list if not specified

        #create height array and temp profile
        if not z.any():
            self.h_max  = h_max
            z           = np.linspace(h_min, self.h_max, h_res+1)
            self.z      = z
            self.z_m    = (z[1:]+z[:-1])/2

        else:
            self.h_max  = z[:-1]
            self.z      = z
            self.z_m    = (z[1:]+z[:-1])/2

        self.dz    = np.diff(self.z)                                    #done: !! needs reformulation for unequidistant grid !!
        self.T     = self.ptModel.get_temperature_array(self.z_m)
        self.p     = self.ptModel.get_pressure_array(self.z_m)

        ####    SETUP GAS-MODEL     #####
        #pass standard gas array defined outside of __init__ if no gas_array was passed:
        if gas_array != None: self.gas_array = gas_array
        #set up gas model from gas_model.py
        self.gas_model = gas_model(self.gas_array, self.ptModel, fnum=fnum)

        ## get optical depth for each gas
        ## kappa is the crossection of gas_model given in 1/meters (kappa/(rho*q))
        ## as a function of height (first dim) and frequency (second dim)
        self.nu, self.kappa = self.gas_model.calculate_cross_section_kv(self.z_m)
        self.dnu            = np.diff(self.nu)[0]
        self.dtau           = self.kappa*self.dz[:, None] # done: ?? may need reformulation for unequidistant grid ??
        self.tau            = np.cumsum(self.dtau, axis = 0)  # done: ?? may need reformulation for unequidistant grid ??

        ##### SET UP OUTPUT VARIABLES FOR MORE EFFICIENT COMPUTATION #####
        self.intensity  = np.zeros(np.shape(self.dtau))
        self.flux       = np.zeros(np.shape(self.dtau))
        self.Planck     = np.zeros(np.shape(self.dtau))
        self.Trms       = np.zeros(np.shape(self.dtau))

    #todo:
    def update_troposphere(self, zmap, ground_temp = None, x_CO2 = None, **kwargs):
        if x_CO2:       self.ptModel.x_CO2  = x_CO2 # mixing ratio of CO2 is taken from phc list if not specified
        if ground_temp:
            #adjust troposphere height if surface temperature has changed
            min_temp = -phc.gamma*h_max + self.ptModel.ground_temperature
            if not h_max: self.h_max = (ground_temperature-min_temp)/phc.gamma
            else:         self.h_max = h_max
            #set new ground temperature in p_t_model
            self.ptModel.set_ground_temperature(ground_temp)

        self.z     = zmap(self.z[1], self.hmax, **kwargs)
        self.z     = np.insert(z,0,0)
        self.z_m   = (self.z[1:]+self.z[:-1])/2
        self.dz    = np.diff(self.z)
        self.T     = self.ptModel.get_temperature_array(self.z_m)

        ####    SETUP GAS-MODEL     #####
        self.gas_model = gas_model(self.gas_array, self.ptModel)

        ## get optical depth for each gas
        ## kappa is the crossection of gas_model given in 1/meters (kappa/(rho*q))
        ## as a function of height (first dim) and frequency (second dim)
        self.nu, self.kappa = self.gas_model.calculate_cross_section_kv(self.z_m)
        self.dnu            = np.diff(self.nu)[0]
        self.dtau           = self.kappa*self.dz[:, None]
        self.tau            = np.cumsum(self.dtau, axis = 0)

    def __get_Transmissivity(self, t1, t2):
        return np.exp(-(t2-t1))

    def __set_Trms_ar(self, r = 1.):
        transmiss_ar  = np.vectorize(self.__get_Transmissivity)
        Trms          = [transmiss_ar(r*self.tau[:,i], r*self.tau[-1,i]) for i in range(0,len(self.nu))]
        self.Trms     = np.moveaxis(np.array(Trms),0,1)
        return True

    def get_Trms_ar(self, r = 1.):
        '''returns the transmissivity of the spectral intensity'''
        self.__set_Trms_ar(r = r)
        return self.Trms

    def __get_Planck(self, T, nu):
        return (2 * phc.h * nu**3 / phc.c**2) / (np.exp(phc.h*nu/(phc.kB*T))-1)

    def __set_Planck_ar(self):
        planck_ar   = np.vectorize(self.__get_Planck)
        lst_planck  = [planck_ar(self.T[i], self.nu) for i in range(0,len(self.T))]
        self.Planck = np.array(lst_planck)
        return True

    def get_Planck_ar(self):
        '''generates 2x2 matrix: returns blackbody radiation as a function
        of temperature (aka height) (first dimension) and frequency (second dimension)'''
        self.__set_Planck_ar()
        return self.Planck

    ##### computing the spectral instensity as a function of height #####
    def __set_spec_intensity(self, init = False):
        if init == False:
            self.__set_Planck_ar()
            self.__set_Trms_ar()
        self.intensity  = self.Planck * self.dtau * self.Trms
        return self.intensity

    def spec_intensity(self, init = False):
        '''returns spectral intensity as a function of height and frequency.
        Set init = True if get_Planck_ar, get_Trms_ar or spec_intensity has been called before'''
        self.__set_spec_intensity(init = init)
        return self.intensity

    def spec_tot_intensity(self, init = False):
        '''returns height integrated spectral intensity as a function of frequency.
        Set init = True if get_Planck_ar, get_Trms_ar or spec_intensity has been called before'''
        self.__set_spec_intensity(init = init)
        return np.sum(self.intensity, axis = 0)+self.Planck[0,:]*self.Trms[0,:]*np.pi

    def tot_intensity(self, init = False):
        '''returns height and frequency integrated intensity.
        Set init = True if get_Planck_ar, get_Trms_ar or spec_intensity has been called before'''
        self.__set_spec_intensity(init = init)
        return np.sum(self.spec_tot_intensity(init = init)*self.dnu)

    ##### Computing the upward flux and outgoing longwave radiation: #####
    def __set_spec_upward_flux(self, init = False):
        r = np.sqrt(np.e)
        if init == False:
            self.__set_Planck_ar()
            self.__set_Trms_ar(r = r)

        self.flux    = np.pi * self.Planck * self.Trms * r * self.dtau
        return True

    def spec_upflux(self, init = False):
        '''returns spectral intensity as a function of height and frequency.
        Set init = True if transmissivity "Trms" has been initialized with sqrt(e).'''
        self.__set_spec_upward_flux(init = init)
        return self.flux

    def spec_tot_upflux(self, init = False):
        '''returns height integrated upward flux as a function of frequency.
        Set init = True if transmissivity "Trms" has been initialized with sqrt(e).'''
        self.__set_spec_upward_flux(init = init)
        return np.sum(self.flux, axis = 0)+self.Planck[0,:]*self.Trms[0,:]*np.pi

    # total outgoing longwave radiation:
    def tot_upflux(self, init = False):
        '''returns outgoing radiation (aka height and frequency integrated upward flux).
        Set init = True if transmissivity "Trms" has been initialized with sqrt(e).'''
        self.__set_spec_upward_flux(init = init)
        return np.sum(self.spec_tot_upflux(init = init)*self.dnu)


    ##### Functions to compute maximum Intensity / Emission height #####
    def get_max_flux(self, init = False):
        '''returns height position and value of maximum intensity
        as a function of frequency'''
        self.__set_spec_upward_flux(init = init)
        max_intens       = np.amax(self.flux, axis = 0)
        index_max_intens = np.argmax(self.flux, axis = 0)
        position_max     = np.array([self.z[i] for i in index_max_intens])
        return position_max, max_intens

    def get_mean_flux(self, init = False):
        '''returns height position and value of mean intensity
        as a function of frequency'''
        self.__set_spec_upward_flux(init = init)
        z_mean_intens = [np.sum(self.z*self.flux[:,i]/np.sum(self.flux[:,i]))
                        for i in range(len(self.nu))]
        index = [np.abs(self.z - z_mean_intens[i]).argmin()
                            for i in range(len(self.nu))]
        mean_intens     = np.array([self.flux[index[i], i] for i in range(len(self.nu))])

        return z_mean_intens, mean_intens

    def get_percentile_flux(self, percentile = 50, init = False):
        '''returns height position and value of percentile intensity
        as a function of frequency. default is percentile = 50'''
        self.__set_spec_upward_flux(init = init)
        self.flux[self.flux == 0.] = np.NAN
        perc_intens       = np.nanpercentile(self.flux, percentile, axis = 0)
        index_perc_intens = [np.abs(self.flux[:,i] - perc_intens[i]).argmin()
                            for i in range(len(self.nu))]
        position_perc     = np.array([self.z[i] for i in index_perc_intens])
        return position_perc, perc_intens

    def get_tau(self, val = 1):
        '''returns height position where tau == 1 from top of atmosphere'''
        index    = [np.abs(self.tau[-1,i]-self.tau[:,i]-val).argmin()
                            for i in range(len(self.nu))]
        position = np.array([self.z[i] for i in index])
        return position

    def get_tau_one(self):
        '''returns height position where tau == 1 from top of atmosphere'''
        index    = [np.abs(self.tau[-1,i]-self.tau[:,i]-1).argmin()
                            for i in range(len(self.nu))]
        position = np.array([self.z[i] for i in index])
        return position
