import math
import physical_constants as phc
import numpy as np


class pt_model:


    def __init__(self, x_CO2 = None):
        self.ground_temperature = 283.15
        self.ground_pressure = 101325.
        self.top_layer_height = 15000.0
        if x_CO2: phc.gas_dic["CO2"][0] = x_CO2

    def set_ground_temperature(self, temp):
        self.ground_temperature = temp

    #Private
    def __get_temperature(self, height):
        """linear temperature profile"""
        temp =  -phc.gamma*height + self.ground_temperature
        return temp

    def get_temperature_array(self, height_array):
        temperature_array = np.vectorize(self.__get_temperature)
        return temperature_array(height_array)

    #Private
    def __get_pressure(self, height):
        """Exponential model from source below
        https://people.clas.ufl.edu/kees/files/AtmosphericPressure.pdf"""
        p0 = self.ground_pressure
        T0 = self.ground_temperature
        pressure = p0*(1.0-phc.gamma*height/T0)**(phc.g/(phc.Rd*phc.gamma))
        return pressure

    def get_pressure_array(self, height_array):
        pressure_array = np.vectorize(self.__get_pressure)
        return pressure_array(height_array)

    #Private
    def __get_air_density(self, height):
        """Using formula for dry air
        https://en.wikipedia.org/wiki/Density_of_air"""
        p = self.__get_pressure(height)
        T = self.__get_temperature(height)
        rho = p/(phc.Rd*T)
        return rho

    def get_air_density_array(self,height_array):
        density_array = np.vectorize(self.__get_air_density)
        return density_array(height_array)

    #Private
    def __calculate_xh2o(self,height):
        """August-Roche-Magnus
        https://en.wikipedia.org/wiki/Vapour_pressure_of_water#Accuracy_of_different_formulations
        """
        Pm = float(self.__get_pressure(height))
        T = float(self.__get_temperature(height))
        x_h20 = 0.5*(611*np.exp(17.27*(T-273.15)/(237.3 + T - 273.15))/Pm)
        return x_h20

    def get_mixing_ration_array(self, gas_array, height):
        """Constant mixing rations for all gasses except H2O
        Other mixing ratios from dictionary of phc.gas_dic
        """
        x_gas_array = []
        for gas in gas_array:
            if gas == "H2O":
                x_h2o = self.__calculate_xh2o(height)
                x_gas_array.append(x_h2o)
            else:
                x_gas_array.append(phc.gas_dic[gas][0])
        return x_gas_array
    # def get_amount_of_substance(self, height):
    #     """Ideal gas law applied"""
    #     V = 1
