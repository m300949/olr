import numpy as np

# Definition of basic physical constants
#
h     = 6.62607015e-34
c     = 299792458.0
kB     = 1.38064852e-23
sigma = 2*(np.pi**5) * kB**4 / (15 * h**3 * c**2)
N_avo = 6.02214076e+23
Rstar = kB*N_avo
g     = 9.81 
gamma =7e-3 # Elapse Rate


#
#------------------------------------------------------------------
#
# typical concentrations of trace gasses
#
x_ar  = 9.332e-3
x_o2  = 0.20944
x_n2  = 0.78083
x_co2 = 415.e-6
x_ch4 = 772.e-9
x_n2o = 334.e-9
x_o3  = 200.e-9
#
#------------------------------------------------------------------
#
# molar masses in g/mol of different elements and molecules
#
m_h = 1.00784
m_c = 12.0107
m_n = 14.0067
m_o = 15.9994
m_ar= 39.948
m_n2  = 2*m_n
m_o2  = 2*m_o
m_o3  = 3*m_o
m_co2 = m_c   + m_o2
m_h2o = m_o   + 2*m_h
m_ch4 = m_c   + 4*m_h
m_n2o = 2*m_n + m_o
#
#------------------------------------------------------------------
#
# Dictionary of gases:
#
gas_dic = {
        "Ar":   [x_ar,m_ar],
        "O2":   [x_o2,m_o2],
        "N2":   [x_n2,m_n2],
        "CO2":  [x_co2,m_co2],
        "CH4":  [x_ch4,m_ch4],
        "H2O":  [0,m_h2o],
        "N2O":  [x_n2o,m_n2o],
        "O3":   [x_o3,m_o3]
        }

#
#------------------------------------------------------------------
#
# dry air molar masses and gas constants
#
m_dry = x_ar*m_ar + x_o2*m_o2 + x_n2*m_n2 + x_co2*m_co2
Rd    = (Rstar/m_dry)*(x_ar+x_o2+x_n2+x_co2) * 1000.  #J/kg/K
