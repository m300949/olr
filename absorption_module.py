"""Calculate and plot absorption cross sections."""
import re

import matplotlib.pyplot as plt
import numpy as np
import typhon as ty
import pyarts
from typhon.plots import styles


def tag2tex(tag):
    """Replace all numbers in a species tag with LaTeX subscripts."""
    return re.sub("([a-zA-Z]+)([0-9]+)", r"\1$_{\2}$", tag)


def calculate_absxsec(
    species="N2O",
    pressure=800e2,
    temperature=300.0,
    fmin=10e9,
    fmax=2000e9,
    fnum=10_000,
    lineshape="LP",
    normalization="RQ",
    verbosity=0
):
    """Calculate absorption cross sections.

    Parameters:
        species (str): Absorption species name.
        pressure (float): Atmospheric pressure [Pa].
        temperature (float): Atmospheric temperature [K].
        fmin (float): Minimum frequency [Hz].
        fmax (float): Maximum frequency [Hz].
        fnum (int): Number of frequency grid points.
        lineshape (str): Line shape model.
                            Available options:
    	                    DP   	 - 	 Doppler profile,
	                        LP   	 - 	 Lorentz profile,
	                        VP   	 - 	 Voigt profile,
	                        SDVP 	 - 	 Speed-dependent Voigt profile,
	                        HTP  	 - 	 Hartman-Tran profile.
        normalization (str): Line shape normalization factor.
                            Available options:
                            VVH  	 - 	 Van Vleck and Huber,
                            VVW  	 - 	 Van Vleck and Weisskopf,
                            RQ   	 - 	 Rosenkranz quadratic,
                            None 	 - 	 No extra normalization.  
        verbosity (int): Set ARTS verbosity (``0`` prevents all output).

    Returns:
        ndarray, ndarray: Frequency grid [Hz], Abs. cross sections [m^2]
    """
    # Create ARTS workspace and load default settings
    ws = pyarts.workspace.Workspace(verbosity=0)
    ws.execute_controlfile("general/general.arts")
    ws.execute_controlfile("general/continua.arts")
    ws.execute_controlfile("general/agendas.arts")
    ws.verbositySetScreen(ws.verbosity, verbosity)

    # We do not want to calculate the Jacobian Matrix
    ws.jacobianOff()

    # Agenda for scalar gas absorption calculation
    ws.Copy(ws.abs_xsec_agenda, ws.abs_xsec_agenda__noCIA)

    # Define absorption species
    ws.abs_speciesSet(species=[species])
    ws.ArrayOfIndexSet(ws.abs_species_active, [0])

    ws.abs_lines_per_speciesReadSpeciesSplitCatalog(
       basename="spectroscopy/Artscat/"
    )

    ws.abs_lines_per_speciesSetLineShapeType(option=lineshape)
    ws.abs_lines_per_speciesSetCutoff(option="ByLine", value=750e9)
    ws.abs_lines_per_speciesSetNormalization(option=normalization)

    # Create a frequency grid
    ws.VectorNLinSpace(ws.f_grid, fnum, fmin, fmax)

    # Throw away lines outside f_grid
    ws.abs_lines_per_speciesCompact()

    # Atmospheric settings
    ws.AtmosphereSet1D()
    ws.stokes_dim = 1

    # Setting the pressure, temperature and vmr
    ws.rtp_pressure = float(pressure)  # [Pa]
    ws.rtp_temperature = float(temperature)  # [K]
    ws.rtp_vmr = np.array([1.0])  # [VMR]
    ws.Touch(ws.rtp_nlte)

    # isotop
    ws.isotopologue_ratiosInitFromBuiltin()

    # Calculate absorption cross sections
    ws.propmat_clearsky_agenda_checked = 1
    ws.lbl_checkedCalc()
    ws.propmat_clearskyInit()
    ws.propmat_clearskyAddLines()

    # Convert abs coeff to cross sections on return
    number_density = pressure / (ty.constants.boltzmann * temperature)
    return (ws.f_grid.value.copy(),
            ws.propmat_clearsky.value.data.data[0, 0, :, 0].copy() / number_density)


calculate_absxsec()
